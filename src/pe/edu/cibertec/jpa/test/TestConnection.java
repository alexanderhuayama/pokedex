package pe.edu.cibertec.jpa.test;

import javax.persistence.EntityManager;

import pe.edu.cibertec.cnx.JPAUtil;

public class TestConnection {
	public static void main(String[] args) {
		invokeEntityManager();
	}

	private static void invokeEntityManager() {
		EntityManager manager = JPAUtil.geEntityManager();
		System.out.println("Conection ready ...");

	}
}
