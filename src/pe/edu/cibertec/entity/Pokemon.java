package pe.edu.cibertec.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tb_pokemon")
public class Pokemon {

	@Id
	@GeneratedValue
	private int id;

	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;

	@Column(name = "vida", nullable = false)
	private int vida;

	@Column(name = "ataque", nullable = false)
	private int ataque;

	@Column(name = "tipo", nullable = false, length = 50)
	private String tipo;

	@Column(name = "fecha_captura")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCaptura;

}
