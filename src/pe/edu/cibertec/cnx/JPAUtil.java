package pe.edu.cibertec.cnx;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	private static EntityManagerFactory FACTORY = Persistence.createEntityManagerFactory("dataBase");
	
	public static EntityManager geEntityManager(){
		System.out.println("Get connection to database ...");
		return FACTORY.createEntityManager();
	}
}
